﻿using System.Web;
using System.Web.Mvc;

namespace AzureToGitSolution
{
    public class FilterConfig
    {
        // first commit
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
